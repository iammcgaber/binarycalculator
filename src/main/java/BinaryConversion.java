public class BinaryConversion {
    private int[] binaryValues = new int[] {128, 64, 32, 16, 8, 4, 2, 1};

    public int binaryToDecimal(int[] binaryAmount) {
        int decimalTotal = 0;
        for (int binary = 0; binary < binaryAmount.length; binary++) {
            if(binaryAmount[binary] == 1) {
                decimalTotal += binaryValues[binary];
            }
        }
        return decimalTotal;
    }

    public int[] decimalToBinary(int value) {
        int[] toBinary = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
        for (int binary = 0; binary < binaryValues.length; binary++) {
            if(binaryValues[binary] <= value) {
                value -= binaryValues[binary];
                toBinary[binary] = 1;
            }
        }
        return toBinary;
    }
}
