import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class DecimalToBinaryTest {
    BinaryConversion binary;

    @Before
    public void setup() {
        binary = new BinaryConversion();
    }

    @Test
    public void decimal0Binary00000000() {
        int[] conversionResult = binary.decimalToBinary(0);
        assertArrayEquals(new int[]{0, 0, 0, 0, 0, 0, 0, 0}, conversionResult);
    }

    @Test
    public void decimal1Binary00000001() {
        int[] conversionResult = binary.decimalToBinary(1);
        assertArrayEquals(new int[]{0, 0, 0, 0, 0, 0, 0, 1}, conversionResult);
    }

    @Test
    public void decimal2Binary00000010() {
        int[] conversionResult = binary.decimalToBinary(2);
        assertArrayEquals(new int[]{0, 0, 0, 0, 0, 0, 1, 0}, conversionResult);
    }

    @Test
    public void decimal3Binary00000011() {
        int[] conversionResult = binary.decimalToBinary(3);
        assertArrayEquals(new int[]{0, 0, 0, 0, 0, 0, 1, 1}, conversionResult);
    }

    @Test
    public void decimal8Binary00001000() {
        int[] conversionResult = binary.decimalToBinary(8);
        assertArrayEquals(new int[]{0, 0, 0, 0, 1, 0, 0, 0}, conversionResult);
    }

    @Test
    public void decimal16Binary00010000() {
        int[] conversionResult = binary.decimalToBinary(16);
        assertArrayEquals(new int[]{0, 0, 0, 1, 0, 0, 0, 0}, conversionResult);
    }

    @Test
    public void decimal32Binary00100000() {
        int[] conversionResult = binary.decimalToBinary(32);
        assertArrayEquals(new int[]{0, 0, 1, 0, 0, 0, 0, 0}, conversionResult);
    }

    @Test
    public void decimal64Binary01000000() {
        int[] conversionResult = binary.decimalToBinary(64);
        assertArrayEquals(new int[]{0, 1, 0, 0, 0, 0, 0, 0}, conversionResult);
    }

    @Test
    public void decimal128Binary10000000() {
        int[] conversionResult = binary.decimalToBinary(128);
        assertArrayEquals(new int[]{1, 0, 0, 0, 0, 0, 0, 0}, conversionResult);
    }

    @Test
    public void decimal129Binary10000001() {
        int[] conversionResult = binary.decimalToBinary(129);
        assertArrayEquals(new int[]{1, 0, 0, 0, 0, 0, 0, 1}, conversionResult);
    }
}
