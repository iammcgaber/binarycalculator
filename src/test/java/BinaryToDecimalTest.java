import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class BinaryToDecimalTest {
    BinaryConversion binary;

    @Before
    public void setup() {
        binary = new BinaryConversion();
    }

    @Test
    public void binary00000000Decimal0() {
        int[] binaryArg = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(0, conversionResult);
    }

    @Test
    public void binary00000001Decimal1() {
        int[] binaryArg = new int[]{0, 0, 0, 0, 0, 0, 0, 1};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(1, conversionResult);
    }

    @Test
    public void binary00000010Decimal2() {
        int[] binaryArg = new int[]{0, 0, 0, 0, 0, 0, 1, 0};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(2, conversionResult);
    }

    @Test
    public void binary00000011Decimal3() {
        int[] binaryArg = new int[]{0, 0, 0, 0, 0, 0, 1, 1};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(3, conversionResult);
    }

    @Test
    public void binary00000100Decimal4() {
        int[] binaryArg = new int[]{0, 0, 0, 0, 0, 1, 0, 0};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(4, conversionResult);
    }

    @Test
    public void binary00000101Decimal5() {
        int[] binaryArg = new int[]{0, 0, 0, 0, 0, 1, 0, 1};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(5
                , conversionResult);
    }

    @Test
    public void binary00000110Decimal6() {
        int[] binaryArg = new int[]{0, 0, 0, 0, 0, 1, 1, 0};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(6
                , conversionResult);
    }

    @Test
    public void binary00001000Decimal6() {
        int[] binaryArg = new int[]{0, 0, 0, 0, 1, 0, 0, 0};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(8
                , conversionResult);
    }

    @Test
    public void binary00010000Decimal16() {
        int[] binaryArg = new int[]{0, 0, 0, 1, 0, 0, 0, 0};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(16
                , conversionResult);
    }

    @Test
    public void binary00100000Decimal32() {
        int[] binaryArg = new int[]{0, 0, 1, 0, 0, 0, 0, 0};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(32
                , conversionResult);
    }

    @Test
    public void binary01000000Decimal64() {
        int[] binaryArg = new int[]{0, 1, 0, 0, 0, 0, 0, 0};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(64
                , conversionResult);
    }

    @Test
    public void binary10000000Decimal128() {
        int[] binaryArg = new int[]{1, 0, 0, 0, 0, 0, 0, 0};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(128
                , conversionResult);
    }

    @Test
    public void binary11111111Decimal255() {
        int[] binaryArg = new int[]{1, 1, 1, 1, 1, 1, 1, 1};
        int conversionResult = binary.binaryToDecimal(binaryArg);
        assertEquals(255
                , conversionResult);
    }
}
